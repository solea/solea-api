/** ENDPOINTS PHOTOS DE PROFILS : Gère l'ajout/suppression de photo de profils */

/* IMPORTS NPM */
import path from 'path';
import multer from 'multer';
import fs from 'fs';

/* IMPORTS API */
import * as ldapService from '../services/ldap.service';




// Répertoire temporaire où sont stockées les images
var upload = multer({
    dest: "images/temp"
  }).single("image");

/**
 * Gestion de l'endpoint d'ajout de photo de profil
 */
export var profilUpload = (req, res) => {

    if (!req.isAuthenticated()) {
        return res.status(401).json({ success: false, message: 'Non authentifié.' });
    }

    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            console.log(err);
            return;
        } else if (err) {
            console.log(err);
            return;
        }

        const tempPath = req.file.path;
        const targetPath = "images/" + req.user.uid + ".png";

        if (path.extname(req.file.originalname).toLowerCase() === ".png") {
            fs.rename(tempPath, targetPath, err => {
                if (err) return res.status(500).json({ success: false, message: 'Une erreur est survenue lors de l\'importation de la photo de profil.' });
                
                console.log('[api] - L\'utilisateur "' + req.user.uid + '" a ajouté sa nouvelle photo de profil.');
                return res.status(200).json({ success: true, message: 'La photo de profil a correctement été importée.' });
            });
        } else {
            fs.unlink(tempPath, err => {
                if (err) return res.status(500).json({ success: false, message: 'Une erreur est survenue lors de l\'importation de la photo de profil.' });

                return res.status(403).json({ success: false, message: 'Seul les images au format PNG sont autorisée.' });
            });
        }
    });
}

/**
 * Gestion de l'endpoint de la suppression de photo de profil
 */
export var profilDelete = (req, res) => {

    if (!req.isAuthenticated()) {
        return res.status(401).json({ success: false, message: 'Non authentifié.' });
    }

    fs.unlink("images/" + req.user.uid + ".png", err => {
        if (err) return res.status(500).json({ success: false, message: 'Une erreur est survenue lors de la suppression de la photo de profil.' });

        console.log('[api] - L\'utilisateur "' + req.user.uid + '" a supprimé sa photo de profil.');
        return res.status(200).json({ success: true, message: 'La photo de profil a correctement été supprimée.' });
    });
}

/**
 * Gestion de l'endpoint de la suppression de photo de profil par UID
 */
export var profilDeleteByUid = async (req, res) => {

    if (!req.isAuthenticated()) {
        return res.status(401).json({ success: false, message: 'Non authentifié.' });
    }

    var isAdmin = false;
    try {
        isAdmin = await ldapService.isAdmin(req.user.uid);
    } catch (error) {
        return res.status(403).json({ success: false, message: 'Impossible de vérifier le statut Administrateur.', error: error });
    }

    if (!isAdmin){
        return res.status(403).json({ success: false, message: 'Privilèges insuffisants. Veuillez contacter votre administrateur.' });
    }


    fs.unlink("images/" + req.params.id + ".png", err => {
        if (err) return res.status(500).json({ success: false, message: 'Une erreur est survenue lors de la suppression de la photo de profil.' });

        console.log('[api] - L\'utilisateur "' + req.user.uid + '" a supprimé la photo de profil de : "' + req.params.id + '"');
        return res.status(200).json({ success: true, message: 'La photo de profil a correctement été supprimée.' });
    });
}