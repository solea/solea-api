/** ENDPOINTS VOIP : Gère les actions sur Asterisk */

/* IMPORTS API */
import * as voipService from '../services/voip.service';


export var clickToCall = (req, res) => {
    if (!req.isAuthenticated()) {
        return res.status(401).json({ success: false, message: 'Non authentifié.' });
    }

    var call = voipService.callExtension("SIP/" + req.user.phone, req.params.id, req.user.phone);

    call.then(
        () => {
            console.log('[api] - L\'utilisateur "' + req.user.uid + '" (n°' + req.user.phone + ') appel le n° : ' + req.params.id);
            return res.json({
                success: true, message: "Appel correctement effectué."
            });
        }
    )
    .catch(function handleErrors(error) {
        return res.status(404).json({ success: false, message: 'Une erreur est survenue lors de l\'appel.', error: error });
    });
}