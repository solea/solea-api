/** ENDPOINTS AUTHENTIFICATIONS : Gère la session d'un utilisateur LDAP */

/* IMPORTS API */
import * as server from '../app'

/**
 * Gestion de l'endpoint d'authentification
 */
export var login = function (req, res, next) {
    server.passport.authenticate('ldapauth', function (err, user, info) {
        if (err) {
            switch (err.code) {
                case 'ETIMEDOUT':
                    return res.status(503).json({ success: false, message: 'Impossible de joindre le serveur d\'authentification.', err, info });
                default:
                    return res.status(500).json({ success: false, message: 'Une erreur est survenue.', err, info });
            }
        }

        req.login(user, loginErr => {

            if (loginErr) {
                return res.status(401).json({ success: false, message: 'Échec de l\'authentification. Identifiant ou mot de passe invalide.', error: loginErr });
            }

            return res.json({success: true, message: 'Authentifié avec succès', user: user});
        });
    })(req, res, next)
};

/**
 * Gestion de l'endpoint de déconnexion
 */
export var logout = (req, res) => {
    if (req.isAuthenticated()) {
        var uid =  req.user.uid;
        req.logOut();
        console.log('[api] - L\'utilisateur LDAP "' + uid + '" vient de se déconnecter.');
        return res.status(200).json({ success: true, message: 'Déconnexion effectuée' });
    } else {
        return res.status(401).json({ success: false, message: 'Non authentifié' });
    }
};