/** ENDPOINTS LDAP : Gère les actions sur le LDAP */

/* IMPORTS NPM */
import fs from 'fs';

/* IMPORTS API */
import * as ldapService from '../services/ldap.service';

/**
 * Gestion de l'endpoint de recherche de contacts.
 */
export var getContacts = (req, res) => {
    if (req.isAuthenticated()) {

        var getContacts = getContactsList();

        getContacts
            .then((contacts) => {
                return res.json({
                    success: true, contacts: Object.assign(contacts)
                });
            })
            .catch(function handleErrors(error) {
                return res.status(404).json({ success: false, message: 'Échec de la recherche. Paramètres incorrectes.', error: error });
            });

    } else {
        return res.status(401).json({ success: false, message: 'Non authentifié' });
    }
}

/**
 * Gestion de l'endpoint de recherche de contact par Uid.
 */
export var getContactUid = (req, res) => {
    if (req.isAuthenticated()) {

        var getContact = getContactByUid(req.params.id);

        getContact
            .then((contact) => {
                return res.json({
                    success: true, contact: Object.assign(contact)
                });
            })
            .catch(function handleErrors(error) {
                return res.status(404).json({ success: false, message: 'Échec de la recherche. Paramètres incorrectes.', error: error });
            });

    } else {
        return res.status(401).json({ success: false, message: 'Non authentifié' });
    }
}

/**
 * Gestion de l'endpoint qui suis-je ?
 */
export var whoIAm = (req, res) => {
    if (req.isAuthenticated()) {
        var getContact = getContactByUid(req.user.uid);

        getContact
            .then((contact) => {
                return res.json({
                    success: true, user: Object.assign(contact)
                });
            })
            .catch(function handleErrors(error) {
                return res.status(404).json({ success: false, message: 'Échec de la recherche. Paramètres incorrectes.', error: error });
            });
    } else {
        return res.status(401).json({ success: false, message: 'Non authentifié' });
    }
};

/**
 * Gestion de l'endpoint de recherche de contacts.
 */
export var getGroups = (req, res) => {
    if (req.isAuthenticated()) {

        var getGroups = ldapService.getGroupsMembers();

        getGroups
            .then((groups) => {
                return res.json({
                    success: true, groups: Object.assign(groups)
                });
            })
            .catch(function handleErrors(error) {
                return res.status(404).json({ success: false, message: 'Échec de la recherche. Paramètres incorrectes.', error: error });
            });

    } else {
        return res.status(401).json({ success: false, message: 'Non authentifié' });
    }
}


/**
 * Gestion de l'endpoint changement de mot de passe
 */
export var changePassword = (req, res) => {
    if (req.isAuthenticated()) {

        var postChangePassword = ldapService.changePassword(req.user.uid, { old: req.body.oldPassword, new: req.body.newPassword });

        postChangePassword
            .then(() => {
                console.log('[api] - L\'utilisateur "' + req.user.uid + '" a changé son mot de passe.')
                return res.json({
                    success: true, message: "Mot de passe changé avec succès."
                });
            })
            .catch(function handleErrors(error) {
                return res.status(404).json({ success: false, message: 'Le changement du mot de passe a échoué. Paramètres incorrectes.', error: error });
            });

    } else {
        return res.status(401).json({ success: false, message: 'Non authentifié.' });
    }
}


/**
 * Gestion de l'endpoint d'ajout de contact
 */
export var addContact = (req, res) => {
    if (req.isAuthenticated()) {

        var checkAdmin = ldapService.isAdmin(req.user.uid);

        checkAdmin.then((isAdmin) => {
            if (isAdmin) {

                var postAddContact = addUser(
                    {
                        surname: req.body.surname,
                        name: req.body.name,
                        job: req.body.job,
                        location: req.body.location
                    }
                );

                postAddContact
                    .then((contact) => {
                        console.log('[api] - L\'utilisateur "' + req.user.uid + '" a ajouté l\'utilisateur : "' + contact.uid + '"');
                        return res.json({
                            success: true, contact: Object.assign(contact)
                        });
                    })
                    .catch(function handleErrors(error) {
                        return res.status(404).json({ success: false, message: 'L\'ajout de l\'utilisateur a échoué. Paramètres incorrectes.', error: error });
                    });

            } else {
                return res.status(403).json({ success: false, message: 'Privilèges insuffisants. Veuillez contacter votre administrateur.' });
            }
        })
            .catch(function handleErrors(error) {
                return res.status(403).json({ success: false, message: 'Impossible de vérifier le statut Administrateur.', error: error });
            });
    } else {
        return res.status(401).json({ success: false, message: 'Non authentifié.' });
    }
}

/**
 * Gestion de l'endpoint d'ajout de contact aux groupes
 */
export var addContactToGroups = (req, res) => {

    if (req.isAuthenticated()) {

        var checkAdmin = ldapService.isAdmin(req.user.uid);

        checkAdmin.then((isAdmin) => {
            if (isAdmin) {

                var postAddContactTogroups = addUserToGroups(
                    req.params.id,
                    req.body.groups
                );

                postAddContactTogroups
                    .then((contact) => {
                        console.log('[api] - L\'utilisateur "' + req.user.uid + '" a ajouté l\'utilisateur "' + contact.uid + '" aux groupes : ' + req.body.groups);
                        return res.json({
                            success: true, contact: Object.assign(contact)
                        });
                    })
                    .catch(function handleErrors(error) {
                        return res.status(404).json({ success: false, message: 'L\'intégration de l\'utilisateur aux groupes a échoué. Paramètres incorrectes.', error: error });
                    });

            } else {
                return res.status(403).json({ success: false, message: 'Privilèges insuffisants. Veuillez contacter votre administrateur.' });
            }
        })
            .catch(function handleErrors(error) {
                return res.status(403).json({ success: false, message: 'Impossible de vérifier le statut Administrateur.', error: error });
            });
    } else {
        return res.status(401).json({ success: false, message: 'Non authentifié.' });
    }
}

/**
 * Gestion de l'endpoint de retrait de contact aux groupes
 */
export var removeContactFromGroups = (req, res) => {
    if (req.isAuthenticated()) {

        var checkAdmin = ldapService.isAdmin(req.user.uid);

        checkAdmin.then((isAdmin) => {
            if (isAdmin) {

                var postAddContactTogroups = removeUserFromGroups(
                    req.params.id,
                    req.body.groups
                );

                postAddContactTogroups
                    .then((contact) => {
                        console.log('[api] - L\'utilisateur "' + req.user.uid + '" a retiré l\'utilisateur "' + contact.uid + '" des groupes : ' + req.body.groups);
                        return res.json({
                            success: true, contact: Object.assign(contact)
                        });
                    })
                    .catch(function handleErrors(error) {
                        return res.status(404).json({ success: false, message: 'L\'intégration de l\'utilisateur aux groupes a échoué. Paramètres incorrectes.', error: error });
                    });

            } else {
                return res.status(403).json({ success: false, message: 'Privilèges insuffisants. Veuillez contacter votre administrateur.' });
            }
        })
            .catch(function handleErrors(error) {
                return res.status(403).json({ success: false, message: 'Impossible de vérifier le statut Administrateur.', error: error });
            });
    } else {
        return res.status(401).json({ success: false, message: 'Non authentifié.' });
    }
}

/**
 * Gestion de l'endpoint modification contact
 */
export var editContact = (req, res) => {
    if (req.isAuthenticated()) {

        var checkAdmin = ldapService.isAdmin(req.user.uid);

        checkAdmin.then((isAdmin) => {
            if (isAdmin) {

                var postEditContact = editUser(req.params.id,
                    {
                        surname: req.body.surname,
                        name: req.body.name,
                        job: req.body.job,
                        location: req.body.location
                    }
                );

                postEditContact
                    .then((contact) => {
                        console.log('[api] - L\'utilisateur "' + req.user.uid + '" a modifié l\'utilisateur : "' + contact.uid + '"');
                        return res.json({
                            success: true, contact: Object.assign(contact)
                        });
                    })
                    .catch(function handleErrors(error) {
                        return res.status(404).json({ success: false, message: 'La modification de l\'utilisateur a échoué. Paramètres incorrectes.', error: error });
                    });

            } else {
                return res.status(403).json({ success: false, message: 'Privilèges insuffisants. Veuillez contacter votre administrateur.' });
            }
        })
            .catch(function handleErrors(error) {
                return res.status(403).json({ success: false, message: 'Impossible de vérifier le statut Administrateur.', error: error });
            });
    } else {
        return res.status(401).json({ success: false, message: 'Non authentifié.' });
    }
}


/**
 * Gestion de l'endpoint réinitialisation d'un mdp d'un contact
 */
export var resetPassword = (req, res) => {
    if (req.isAuthenticated()) {

        var checkAdmin = ldapService.isAdmin(req.user.uid);

        checkAdmin.then((isAdmin) => {
            if (isAdmin) {

                var postResetPassword = ldapService.resetPassword(req.params.id);

                postResetPassword
                    .then(() => {
                        console.log('[api] - L\'utilisateur "' + req.user.uid + '" a réinitialisé le mot de passe de l\'utilisateur : "' + req.params.id + '"');
                        return res.json({
                            success: true, message: "Le mot de passe a correctement été réinitialisé."
                        });
                    })
                    .catch(function handleErrors(error) {
                        return res.status(404).json({ success: false, message: 'La réinitialisation du mot de passe a échoué. Paramètres incorrectes.', error: error });
                    });

            } else {
                return res.status(403).json({ success: false, message: 'Privilèges insuffisants. Veuillez contacter votre administrateur.' });
            }
        })
            .catch(function handleErrors(error) {
                return res.status(403).json({ success: false, message: 'Impossible de vérifier le statut Administrateur.', error: error });
            });
    } else {
        return res.status(401).json({ success: false, message: 'Non authentifié.' });
    }
}


/**
 * Gestion de l'endpoint des privilèges admin d'un contact
 */
export var grantAdmin = (req, res) => {
    if (req.isAuthenticated()) {
        var checkAdmin = ldapService.isAdmin(req.user.uid);

        checkAdmin.then((isAdmin) => {
            if (isAdmin) {
                var setAdmin = setAdminContact(req.params.id, req.body.admin);

                setAdmin
                    .then((contact) => {
                        if (contact.admin) {
                            console.log('[api] - L\'utilisateur "' + req.user.uid + '" a donné les privilèges administrateur de l\'utilisateur : "' + contact.uid + '"');
                        } else {
                            console.log('[api] - L\'utilisateur "' + req.user.uid + '" a retiré les privilèges administrateur de l\'utilisateur : "' + contact.uid + '"');
                        }

                        return res.json({
                            success: true, contact: Object.assign(contact)
                        });
                    })
                    .catch(function handleErrors(error) {
                        return res.status(404).json({ success: false, message: 'Échec de la modification. Paramètres incorrectes.', error: error });
                    });
            } else {
                return res.status(403).json({ success: false, message: 'Privilèges insuffisants. Veuillez contacter votre administrateur.' });
            }
        })
            .catch(function handleErrors(error) {
                return res.status(403).json({ success: false, message: 'Impossible de vérifier le statut Administrateur.', error: error });
            });
    } else {
        return res.status(401).json({ success: false, message: 'Non authentifié' });
    }
}


/**
 * Gestion de l'endpoint suppression d'un contact
 */
export var deleteContact = (req, res) => {
    if (req.isAuthenticated()) {

        var checkAdmin = ldapService.isAdmin(req.user.uid);

        checkAdmin.then(async (isAdmin) => {
            if (isAdmin) {

                var user;
                try {
                    user = await getContactByUid(req.params.id);
                } catch (e) {
                    reject(e);
                    return;
                }

                // Supprime la photo de profil
                if (fs.existsSync("images/" + user.uid + ".png")) {
                    fs.unlink("images/" + user.uid + ".png", err => {
                        if (err) return res.status(500).json({ success: false, message: 'Une erreur est survenue lors de la suppression de la photo de profil.' });
                    });
                }

                var groups = [];
                user.groups.forEach(group => {
                    groups.push(group.gname);
                });

                // Supprime l'utilisateur des groupes auxquels il appartient
                var delContactFromGroups = removeUserFromGroups(user.uid, groups);

                delContactFromGroups
                    .then(() => {
                        var delContact = ldapService.removeUser(user.uid);

                        delContact
                            .then(() => {
                                console.log('[api] - L\'utilisateur "' + req.user.uid + '" a supprimé l\'utilisateur : "' + req.params.id + '"');
                                return res.json({
                                    success: true, message: "Contact correctement retiré de l'annuaire LDAP."
                                });
                            })
                            .catch(function handleErrors(error) {
                                return res.status(404).json({ success: false, message: 'Échec de la suppression. Paramètres incorrectes.', error: error });
                            });
                    })
                    .catch(function handleErrors(error) {
                        return res.status(404).json({ success: false, message: 'Échec de la suppression. Paramètres incorrectes.', error: error });
                    });
            } else {
                return res.status(403).json({ success: false, message: 'Privilèges insuffisants. Veuillez contacter votre administrateur.' });
            }
        })
            .catch(function handleErrors(error) {
                return res.status(403).json({ success: false, message: 'Impossible de vérifier le statut Administrateur.', error: error });
            });
    } else {
        return res.status(401).json({ success: false, message: 'Non authentifié' });
    }
}


export function getContactByUid(uid) {
    return new Promise(async function (resolve, reject) {

        var user = {}
        try {
            user = await ldapService.getUserByUid(uid);
        } catch (e) {
            reject(e);
            return;
        }

        var groups = [];
        try {
            groups = await ldapService.getGroupsByUid(uid);
        } catch (e) {
            reject(e);
            return;
        }

        user.admin = groups.some(e => e.gname === 'web_adm') ? true : false;

        user.photo = '';
        if (fs.existsSync("images/" + user.uid + ".png")) {
            user.photo = "images/" + user.uid + ".png";
        }

        user.groups = groups;

        resolve(user);
    });
}

/**
 * Ajoute un utilisateur et retourne l'utilisateur associé
 * @param {*} user 
 */
function addUser(user) {
    return new Promise(async function (resolve, reject) {

        var contact;
        try {
            contact = await ldapService.addUserAsync(user);
        } catch (e) {
            reject(e);
            return;
        }

        var resolveduser;
        try {
            resolveduser = await getContactByUid(contact.uid);
        } catch (e) {
            reject(e);
            return;
        }

        resolve(resolveduser);
    });
}


/**
 * Ajoute un utilisateur aux groupes et retourne l'utilisateur associé
 * @param {*} uid : uid de l'utilisateur modifié
 * @param {*} groups : groupes dans lequel l'utilisateur va être ajouté
 */
function addUserToGroups(uid, groups) {
    return new Promise(async function (resolve, reject) {

        groups.forEach(async group => {
            try {
                await ldapService.addUserToGroup(uid, group);
            } catch (e) {
                reject(e);
                return;
            }
        });

        var resolveduser;
        try {
            resolveduser = await getContactByUid(uid);
        } catch (e) {
            reject(e);
            return;
        }

        resolve(resolveduser);
    });
}

/**
 * Retire un utilisateur des groupes et retourne l'utilisateur associé
 * @param {*} uid : uid de l'utilisateur modifié
 * @param {*} groups : groupes dans lequel l'utilisateur va être retiré
 */
function removeUserFromGroups(uid, groups) {
    return new Promise(async function (resolve, reject) {


        groups.forEach(async group => {
            try {
                await ldapService.removeUserFromGroup(uid, group);
            } catch (e) {
                reject(e);
                return;
            }
        });

        var resolveduser;
        try {
            resolveduser = await getContactByUid(uid);
        } catch (e) {
            reject(e);
            return;
        }

        resolve(resolveduser);
    });
}

/**
 * Modifie un utilisateur et retourne l'utilisateur associé
 * @param {*} user 
 */
function editUser(uid, user) {
    return new Promise(async function (resolve, reject) {

        var contact;
        try {
            contact = await ldapService.editUserAsync(uid, user);
        } catch (e) {
            reject(e);
            return;
        }

        var resolveduser;
        try {
            resolveduser = await getContactByUid(contact.uid);
        } catch (e) {
            reject(e);
            return;
        }

        resolve(resolveduser);
    });
}

/**
 * Ajoute ou retire les privilèges admin d'un contact
 */
function setAdminContact(uid, isAdmin) {
    return new Promise(async function (resolve, reject) {

        if (isAdmin) {
            try {
                await ldapService.addUserToGroup(uid, "web_adm");
            } catch (e) {
                reject(e);
                return;
            }
        } else {
            try {
                await ldapService.removeUserFromGroup(uid, "web_adm");
            } catch (e) {
                reject(e);
                return;
            }
        }

        var resolveduser;
        try {
            resolveduser = await getContactByUid(uid);
        } catch (e) {
            reject(e);
            return;
        }

        resolve(resolveduser);
    });
}

async function getContactsList() {
    return new Promise(async function (resolve, reject) {

        var users = [];
        try {
            users = await ldapService.getUsers();
        } catch (e) {
            reject(e);
            return;
        }

        var groups = [];
        try {
            groups = await ldapService.getGroupsMembers();
        } catch (e) {
            reject(e);
            return;
        }

        var contacts = [];
        users.forEach((user) => {
            var groupsuser = [];

            groups.forEach((group) => {
                group.members.forEach((member) => {
                    if (user.uid instanceof Array) {
                        if (user.uid.find(function (uid) { return uid === member.uid })) {
                            groupsuser.push({ guid: group.guid, gname: group.gname });
                        }
                    } else {
                        if (user.uid === member.uid) {
                            groupsuser.push({ guid: group.guid, gname: group.gname });
                        }
                    }
                });
            });

            user.admin = groupsuser.some(e => e.gname === 'web_adm') ? true : false;

            user.photo = '';
            if (fs.existsSync("images/" + user.uid + ".png")) {
                user.photo = "images/" + user.uid + ".png";
            }

            user.groups = groupsuser;

            contacts.push(user);
        });

        resolve(contacts);
    });
}