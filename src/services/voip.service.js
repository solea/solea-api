/** SERVICE PROPOSANT LES FONCTIONS D'APPELS SUR ASTERISK */

/** IMPORTS NPM */
import AMI from 'asterisk-manager';

/** IMPORTS API */
import config from '../configuration'

var isConnected = false;

var client = new AMI(config.voip.port, config.voip.host, config.voip.user, config.voip.password, true);
client.keepConnected();

client.on('error', function(err) {
    isConnected = false;
    console.log('[server] - Connexion au serveur Asterisk échoué. Impossible de se connecter à partir de "' + config.voip.host + ':' + config.voip.port + '". ' + err);
});

client.on('connect', () => {
    isConnected = true;
    console.log('[server] - Attaché au serveur Asterisk "' + config.voip.host + ':' + config.voip.port + '".');
});

client.on('disconnect', () => {
    isConnected = false;
    console.log('[server] - La connexion au serveur Asterisk "' + config.voip.host + ':' + config.voip.port + '" a été fermée.');
});

/**
 * Appel un numéro (extension) depuis un channel (son numéro).
 * @param {string} channel : son numéro au format 'SIP/myphone'
 * @param {number} exten : l'extension/numéro à appeler au format 1234
 * @param {number} callerId : le numéro/compte que l'on souhaite présenter
 */
export function callExtension (channel, exten, callerId) {

    return new Promise(function (resolve, reject) {
        
        if (!isConnected){
            reject("Client déconnecté.");
        }

        client.action({
            'action':'originate',
            'channel': channel,
            'context':'internal',
            'exten': exten,
            'priority':1,
            'CallerID': callerId
        }, (err, res) => {
            if (err) {
                console.log(err);
                reject(err);
            }
            resolve(res);
        });
    });
}