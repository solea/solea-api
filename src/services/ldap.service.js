/** SERVICE PROPOSANT LES FONCTIONS DE TRAITEMENT SUR LE LDAP */

/** IMPORTS NPM */
import ldap from 'ldapjs';

/** IMPORTS API */
import config from '../configuration'

/** Session cliente au LDAP */
const client = ldap.createClient(
    {
        url: config.ldap.url,
        reconnect: true
    });

client.on('error', (err) => {
    console.error('[server] - Connexion au LDAP échoué. Impossible de se connecter au LDAP à partir de "' + config.ldap.url + '". ' + err);
});
client.on('connect', () => bind());


/**
 * Connection au LDAP en mode administrateur. Instancie l'attribut 'client'.
 * @example
 * client.on('connect', () => bind());
 */
function bind() {
    client.bind(config.ldap.adminDn, config.ldap.adminPassword, (err) => {
        if (err) {
            console.error('[server] - Bind LDAP échoué. Les accès administrateurs ne sont pas valides.' + 'error:' + err);
        }
        else {
            console.log('[server] - Attaché au LDAP "' + config.ldap.url + '".');
        }
    });
}

/**
 * Recherche un utilisateur du LDAP par son UID
 * @returns {({guid:string, gname:string}[])} Un utilisateur du LDAP
 * @example 
 * var users = await getUserByUid('xavier.brassoud@solea3.fr');
 */
export function getUserByUid(uid) {
    return new Promise(async function (resolve, reject) {
        var user = null;
        var entries = [];

        try {
            entries = await searchByOptionsAsync(
                {
                    filter: '(&(objectClass=person)(uid=' + uid + '))',
                    scope: 'sub'
                }
            );
        } catch (e) {
            reject(e);
            return;
        }

        if (entries.length > 0) {
            user = {
                id: entries[0].uidNumber,
                uid: entries[0].uid,
                surname: entries[0].sn,
                name: entries[0].givenName,
                mail: entries[0].mail,
                phone: entries[0].telephoneNumber,
                mobile: entries[0].mobile,
                job: entries[0].title,
                location: entries[0].l
            };

            resolve(user);

        } else {
            reject("Utilisateur introuvable.");
        }
    });
}

/**
 * Recherche tous les utilisateurs du LDAP
 * @returns {({guid:string, gname:string}[])} La liste des utilisateurs du LDAP
 * @example 
 * var users = await getUsers();
 */
export function getUsers() {
    return new Promise(async function (resolve, reject) {
        var users = [];
        var entries = [];

        try {
            entries = await searchByOptionsAsync(
                {
                    filter: '(objectClass=person)',
                    scope: 'sub'
                }
            );
        } catch (e) {
            reject(e);
            return;
        }

        entries.forEach((user) => {
            users.push({
                id: user.uidNumber,
                uid: user.uid,
                surname: user.sn,
                name: user.givenName,
                mail: user.mail,
                phone: user.telephoneNumber,
                mobile: user.mobile,
                job: user.title,
                location: user.l
            })
        });

        resolve(users);
    });
}


/**
 * Recherche les groupes auxquels appartient le compte
 * @param {string} uid UID du compte LDAP
 * @returns {({guid:string, gname:string}[])} La liste des groupes du compte
 * @example 
 * var groups = await getGroupsByUid('toto');
 */
export function getGroupsByUid(uid) {
    return new Promise(async function (resolve, reject) {
        var groups = [];
        var entries = [];

        try {
            entries = await searchByOptionsAsync(
                {
                    filter: '(&(objectClass=posixGroup)(memberUid=' + uid + '))',
                    scope: 'sub',
                    attributes: ['gidNumber', 'cn']
                }
            );
        } catch (e) {
            reject(e);
            return;
        }

        entries.forEach((group) => {
            groups.push({ guid: group.gidNumber, gname: group.cn })
        });

        resolve(groups);
    });
}


/**
 * Indique si le compte a les privilèges administrateur ou non
 */
export function isAdmin(uid) {
    return new Promise(async function (resolve, reject) {

        var entries = [];

        try {
            entries = await searchByOptionsAsync(
                {
                    filter: '(&(objectClass=posixGroup)(cn=web_adm)(memberUid=' + uid + '))',
                    scope: 'sub',
                    attributes: ['gidNumber', 'cn']
                }
            );
        } catch (e) {
            reject(e);
            return;
        }

        resolve(entries.length > 0);
    });
}


/**
 * Recherche les utilisateurs appartenant à chaque groupes
 * @returns {({guid:string, gname:string}[])} La liste des groupes et des utilisateurs associés
 * @example 
 * var groups = await getGroupsMembers();
 */
export function getGroupsMembers() {
    return new Promise(async function (resolve, reject) {
        var groups = [];
        var entries = [];

        try {
            entries = await searchByOptionsAsync(
                {
                    filter: '(objectClass=posixGroup)',
                    scope: 'sub',
                    attributes: ['gidNumber', 'cn', 'memberUid']
                }
            );
        } catch (e) {
            reject(e);
            return;
        }

        entries.forEach((group) => {
            var members = [];

            if (group.memberUid !== undefined){
                if (group.memberUid instanceof Array){
                    group.memberUid.forEach((member) => {
                        members.push({ uid: member });
                    });
                } else {
                    members.push({ uid: group.memberUid });
                }
            }

            groups.push({ guid: group.gidNumber, gname: group.cn, members: members });
        });

        resolve(groups);
    });
}


/**
 * Recherche un ensemble de valeurs dans le LDAP
 * @param {*} options Objet 'options' formaté : http://ldapjs.org/client.html#search
 * @returns {(*[])} Une liste d'entrés correspondant à la recherche
 * @example 
 * var entries = await searchByOptionsAsync(
 * {
 *     filter: '(&(objectClass=posixGroup)(memberUid=' + uid + '))',
 *     scope: 'sub',
 *     attributes: ['gidNumber', 'cn']
 * });
 */
function searchByOptionsAsync(options) {
    return new Promise(async (resolve, reject) => {

        // La liste des groupes
        var entries = [];

        client.search(config.ldap.dn, options, function (err, res) {
            if (err) {
                reject('error:' + err);
                return;
            }

            res.on('searchEntry', function (entry) {
                entries.push(entry.object);
            });
            res.on('error', function (err) {
                reject('La requête a échouée.' + 'error:' + err);
                return;
            });
            res.on('end', function (result) {
                //console.log('entries: ' + JSON.stringify(entries));
                resolve(entries);
            });
        });
    });
}


/**
 * Change password
 */
export async function changePassword(uid, passwords) {
    return new Promise(async (resolve, reject) => {

        // Vérification de l'existence de l'utilisateur
        var user;
        try {
            user = await searchByOptionsAsync(
                {
                    filter: '(&(objectClass=person)(uid=' + uid + '))',
                    scope: 'sub'
                }
            );
        } catch (e) {
            reject(e);
            return;
        }

        if (passwords.old != user[0].userPassword){
            reject("L'ancien mot de passe n'est pas valide.");
            return;
        }

        var change = new ldap.Change({
            operation: 'replace',
            modification: {
                userPassword: passwords.new
            }
        });


        client.modify('uid=' + user[0].uid + ',' + config.ldap.ouUser, change, function (err) {
            if (err) {
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
}


/**
 * Ajoute un utilisateur dans le LDAP.
 * @param {*} user Objet 'user' formaté : http://ldapjs.org/client.html#addContact
 * @returns {(*[])} L'objet user ajouté.
 * @example 
 * 
 * var user = {
 *      surname: "BRASSOUD",
 *      name: "Xavier",
 *      job: "Développeur Web",
 *      location: "Aix-les-bains"
 * };
 * await addUserAsync(user);
 */
export async function addUserAsync(user) {
    return new Promise(async (resolve, reject) => {
        var users = [];
        try {
            users = await getUsers();
        } catch (e) {
            reject(e);
            return;
        }

        var login = generateLogin(user, users);

        var mail = generateMail(user, users);

        var uidNumber = generateUidNumber(users);

        var phone = generatePhone(users);


        var userMapped = {
            objectClass: ['top', 'person', 'inetOrgPerson', 'posixAccount', 'shadowAccount'],
            shadowMin: 0,
            shadowMax: 99999,
            shadowLastChange: 1000,
            shadowWarning: 7,
            userPassword: config.ldap.defaultUserPassword,
            cn: user.name + ' ' + user.surname,
            sn: user.surname,
            givenName: user.name,
            uid: login,
            uidNumber: uidNumber,
            gidNumber: uidNumber,
            homeDirectory: '/home/' + login,
            title: user.job,
            preferredLanguage: 'Français',
            loginShell: '/bin/bash',
            mail: mail,
            l: user.location,
            telephoneNumber: phone,
            mobile: phone,
            gecos: mail
        };

        client.add('uid=' + login + ',' + config.ldap.ouUser, userMapped, function (err) {
            if (err) {
                reject('error:' + err);
            }
            else {
                resolve(userMapped);
            }
        });
    });
}


/**
 * Modifie un utilisateur dans le LDAP.
 * @param {*} user Objet 'user' formaté : http://ldapjs.org/client.html#addContact
 * @returns {(*[])} L'objet user ajouté.
 * @example 
 * 
 * var user = {
 *      surname: "BRASSOUD",
 *      name: "Xavier",
 *      job: "Développeur Web",
 *      location: "Aix-les-bains"
 * };
 * await editUserAsync(user);
 */
export async function editUserAsync(uid, user) {

    return new Promise(async (resolve, reject) => {
        
        // Vérification de l'existence de l'utilisateur
        var ldapUser;
        try {
            ldapUser = await getUserByUid(uid)
        }
        catch (ex) {
            reject(ex);
            return;
        }

        var isNameChanged = false;
        var properties = {};
        if (user.surname) {
            properties.sn = user.surname;
            ldapUser.surname = user.surname;
            isNameChanged = true;
        }
        if (user.name) {
            properties.givenName = user.name;
            ldapUser.name = user.name;
            isNameChanged = true;
        }
        if (user.job) {
            properties.title = user.job;
        }
        if (user.location) {
            properties.l = user.location;
        }

        // On touche au nom/prénom donc on UPDATE tous les paramètres en relations (à par ses UID)
        if (isNameChanged){
            properties.cn = ldapUser.surname + ' ' + ldapUser.name;
        }

        if (properties) {

            for(var propertyName in properties) {
                
                var change = new ldap.Change({
                    operation: 'replace',
                    modification: {
                        [propertyName]: properties[propertyName]
                    }
                });

                client.modify('uid=' + ldapUser.uid + ',' + config.ldap.ouUser, change, function (err) {
                    if (err) {
                        reject(err);
                        return;
                    }
                    else {
                        resolve(ldapUser);
                    }
                });
            }
            
        }
        else {
            reject("Aucune valeur n'a été modifiée.");
            return;
        }
    });
}

function generateLogin(user, users) {
    var login = "";

    if (user.surname.length >= 7) {
        login = user.surname.slice(0, 7);
    } else {
        login = user.surname;
    }

    if (login.length >= 7) {
        login = login + user.name.slice(0, 1);
    } else {
        login = login + user.name.slice(0, 7 - login.length);
    }

    if (users.some(e => e.uid.toLowerCase() === login.toLowerCase())) {

        var index = 0;
        while (users.some(e => e.uid.toLowerCase() === login.toLowerCase() + index)) {
            index = index + 1;
        }

        login = login + index;
    }

    return login.toLowerCase();
}

function generateMail(user, users) {

    var mail = user.name + "." + user.surname + "@solea3.fr";

    if (users.some(e => e.mail.toLowerCase() === mail.toLowerCase())) {

        var index = 0;
        while (users.some(e => e.mail.toLowerCase() === user.name.toLowerCase() + "." + user.surname.toLowerCase() + "." + index + "@solea3.fr")) {
            index = index + 1;
        }

        mail = user.name + "." + user.surname + "." + index + "@solea3.fr";
    }

    return mail.toLowerCase();
}

function generateUidNumber(users) {

    var uid = 1000;

    if (users.some(e => e.id === uid.toString())) {

        while (users.some(e => e.id === uid.toString())) {
            uid = uid + 1;
        }
    }

    return uid;
}

function generatePhone(users) {

    var phone = 30;

    if (users.some(e => e.phone == phone)) {

        while (users.some(e => e.phone == phone)) {
            phone = phone + 1;
        }
    }

    return phone;
}

/**
 * Reset password
 */
export async function resetPassword(uid) {
    return new Promise(async (resolve, reject) => {

        // Vérification de l'existence de l'utilisateur
        var user;
        try {
            user = await getUserByUid(uid)
        }
        catch (ex) {
            reject(ex);
            return;
        }

        var change = new ldap.Change({
            operation: 'replace',
            modification: {
                userPassword: config.ldap.defaultUserPassword
            }
        });

        client.modify('uid=' + user.uid + ',' + config.ldap.ouUser, change, function (err) {
            if (err) {
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
}

/**
 * Ajoute un utilisateur existant dans un groupe.
 */
export async function addUserToGroup(uid, group) {
    return new Promise(async (resolve, reject) => {

        // Vérification de l'existence de l'utilisateur
        try {
            await getUserByUid(uid)
        }
        catch (ex) {
            reject(ex);
            return;
        }

        var change = new ldap.Change({
            operation: 'add',
            modification: {
                memberUid: uid
            }
        });

        client.modify('cn=' + group + ',ou=Groups,dc=solea3,dc=fr', change, function (err) {
            if (err) {
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
}

/**
 * Retire un utilisateur existant d'un groupe.
 */
export async function removeUserFromGroup(uid, group) {
    return new Promise(async (resolve, reject) => {

        // Vérification de l'existence de l'utilisateur
        try {
            await getUserByUid(uid)
        }
        catch (ex) {
            reject(ex);
            return;
        }

        var change = new ldap.Change({
            operation: 'delete',
            modification: {
                memberUid: uid
            }
        });

        client.modify('cn=' + group + ',ou=Groups,dc=solea3,dc=fr', change, function (err) {
            if (err) {
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
}

/**
 * Retire un utilisateur existant
 */
export async function removeUser(uid) {
    return new Promise(async (resolve, reject) => {

        // Vérification de l'existence de l'utilisateur
        var user;
        try {
            user = await getUserByUid(uid)
        }
        catch (ex) {
            reject(ex);
            return;
        }

        client.del('uid=' + user.uid + ',' + config.ldap.ouUser, function (err) {
            if (err) {
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
}
