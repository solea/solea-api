/* IMPORTS NPM */
import express from 'express';
import session from 'express-session';
import uuid from 'uuid/v4';
import passport from 'passport';
import LdapStrategy from 'passport-ldapauth';
import bodyParser from 'body-parser';
import fs from 'fs';
import https from 'https';
const FileStore = require('session-file-store')(session);

/* IMPORTS API */
import * as config from './configuration'
import * as auth from './endpoints/authentication.actions';
import * as ldap from './endpoints/ldap.actions';
import * as pictures from './endpoints/profil-pictures.actions'
import * as voip from './endpoints/voip.actions'


/* PARAMÈTRES GLOBAUX */
const apiRoot = '/api/v' + config.api.currentVersion + '/';


/* PARAMÈTRES DU MIDDLEWARE */

// Stratégies d'authentification au LDAP
var getLDAPConfiguration = function (req, callback) {
    process.nextTick(function () {
        var opts = {
            server: {
                url: config.ldap.url,
                bindDn: config.ldap.adminDn,
                bindCredentials: config.ldap.adminPassword,
                searchBase: config.ldap.ouUser,
                searchFilter: `uid=${req.body.uid}`
            },
            usernameField: 'uid',
            passwordField: 'password'
        };
   
        callback(null, opts);
    });
};

passport.use(new LdapStrategy(getLDAPConfiguration,
    function (user, done) {

        var getGroups = require('./services/ldap.service').getGroupsByUid(user.uid);

        getGroups
            .then((groups) => {
                return done(null,{
                    id: user.uidNumber,
                    uid: user.uid,
                    surname: user.sn,
                    name: user.givenName,
                    mail: user.mail,
                    phone: user.telephoneNumber,
                    mobile: user.mobile,
                    job: user.title,
                    location: user.l,
                    admin: groups.some(e => e.gname === 'web_adm') ? true : false,
                    groups: groups
                });
            });

        console.log('[api] - L\'utilisateur LDAP "' + user.uid + '" vient de s\'authentifier.')
    }));

// Associe l'utilisateur connecté au UUID de session (fichier.json)
passport.serializeUser((user, done) => {
    done(null, user);
});
// Récupère l'utilisateur connecté avec l'UUID de session (fichier.json)
passport.deserializeUser(function (id, done) {
    done(null, id);
})

// Création du serveur
const app = express();

// Configuration du middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({
    genid: (req) => {
        return uuid(); // Génère un nouveau token (cookie de session)
    },
    store: new FileStore(), // Sauvegarde le cookie en local
    secret: config.api.hashSessionSecret, // Hash l'UUID
    resave: false,
    saveUninitialized: false // On ne veux pas créer de fichier de session si l'utilisateur n'est pas authentifié
}));
app.use(passport.initialize());
app.use(passport.session());

// Autorisation de toutes les requêtes en provenance du frontend "http://localhost:4200" à destination de ce serveur (Enabling CORS)
// /!\ ATTENTION : ne fonctionne pas si on passe le wildcard '*' à la place de l'origin (problème de sécurité) dans "Access-Control-Allow-Origin"
//     -> https://stackoverflow.com/a/45723981/9414882
app.use(function(req, res, next) {
    if (config.api.trustedConsumers.indexOf(req.headers.origin) !== -1) {
        res.header("Access-Control-Allow-Origin", req.headers.origin);
        res.header('Access-Control-Allow-Credentials', true);
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    }
    next();
  });

// Chemin des photos de profils
app.use("/images", express.static("images"));

// Exposition des variables aux endpoints
export { passport };


/* ENDPOINTS */

// Endpoint : Authentification
app.post(apiRoot + 'auth', auth.login);

// Endpoint : Déconnexion
app.get(apiRoot + 'logout', auth.logout);

// Endpoint : Qui suis-je ?
app.get(apiRoot + 'whoiam', ldap.whoIAm);

// Endpoint : Changer de mot de passe
app.post(apiRoot + 'changePassword', ldap.changePassword);


// Endpoint : Obtenir un contact par son UID
app.get(apiRoot + 'contact/:id', ldap.getContactUid);

// Endpoint : Liste des contacts
app.get(apiRoot + 'contacts', ldap.getContacts);

// Endpoint : Liste des groupes
app.get(apiRoot + 'groups', ldap.getGroups);


// Enpoint : Ajouter contact
app.post(apiRoot + 'addContact', ldap.addContact);

// Endpoint : Ajouter un contact aux groupes passé en paramètres
app.post(apiRoot + 'addContactGroups/:id', ldap.addContactToGroups);

// Endpoint : Supprimer un contact des groupes passé en paramètres
app.post(apiRoot + 'deleteContactGroups/:id', ldap.removeContactFromGroups);

// Endpoint : Donne ou retire les privilèges admin d'un contact
app.post(apiRoot + 'grantAdmin/:id', ldap.grantAdmin);

// Enpoint : Modifier contact
app.post(apiRoot + 'editContact/:id', ldap.editContact);

// Enpoint : Supprimer contact
app.delete(apiRoot + 'deleteContact/:id', ldap.deleteContact);

// Endpoint : Réinitialise le mot de passe d'un contact au valeurs par défaut
app.post(apiRoot + 'resetPassword/:id', ldap.resetPassword);


// Endpoint : Importer sa photo de profil
app.post(apiRoot + 'upload', pictures.profilUpload);

// Endpoint : Supprimer sa photo de profil
app.delete(apiRoot + 'deleteProfil', pictures.profilDelete);

// Endpoint : Supprimer une photo de profil par UID
app.delete(apiRoot + 'deleteProfil/:id', pictures.profilDeleteByUid);


// Endpoint : Click to call !
app.post(apiRoot + 'call/:id', voip.clickToCall);


/* LANCEMENT DU SERVEUR */
if (config.https) {
    https.createServer({
        key: fs.readFileSync(config.https.key),
        cert: fs.readFileSync(config.https.cert)
    }, app).listen(config.port, () => {
        console.log(`[server] - Running on https://127.0.0.1:${config.port}`)
    });
} else {
    app.listen(config.port, () => {
        console.log(`[server] - Running on http://127.0.0.1:${config.port}`)
    });
}
