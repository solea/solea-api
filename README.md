# API REST Solea

L'API REST permet de s'interfacer au LDAP et de gérer les authentifications avec différents *Endpoints*.

## Compiler le serveur

Exécutez `npm run dev`. Rendez-vous à l'adresse http://localhost:5000/ avec votre navigateur favori.

### Test avec un container LDAP (Docker)

Exécutez :

``` bash
docker-compose up -d
```

Puis vérifiez que le container fonctionne :
``` bash
ldapsearch -x -H ldap://192.168.99.100 -b dc=solea,dc=fr -D "cn=admin,dc=solea3,dc=fr" -w Solea123-
```

### Déployer l'API en production

Installez les packages PM2 et Babel-CLI :
``` bash
npm i -g pm2
npm i -g babel-cli
```

Clonez ce dépôt, puis exécutez :
``` bash
npm install
npm run build
```

Enfin exécutez pm2 :
``` bash
 pm2 start ecosystem.json
 ```


## Endpoints

### Authentification

Permet de s'authentifier via le LDAP.

* URL : http://localhost:5000/api/v1/auth

* Méthode : **POST**
  
* Données en paramètres :
  ``` JSON
  {
    "uid": "UID",
    "password": "PASSWORD"
  }
  ```
  * Obligatoires
    * *UID* = Identifiant de l'utilisateur
    * *PASSWORD* = Mot de passe de l'utilisateur

* Succès de la requête :
  
  Retourne un **cookie de session**.
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true","message":"Authentification effectuée","user": UTILISATEUR_LDAP }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Echec de l'authentification","error":{"message":"Raison"}}
  ```

* Exemple d'appel :
``` bash
curl -X POST http://localhost:5000/api/v1/auth -c cookie-file.txt -H 'Content-Type: application/json' -d '{"uid":"brassoux", "password":"password"}'

Response :
{
  {
    "success": true,
    "message": "Authentifié avec succès",
    "user": {
      "id": "1003",
      "uid": [
          "brassoux",
          "Xavier.Brassoux@solea3.fr"
      ],
      "name": "xavier brassoud",
      "mail": "Xavier.Brassoux@solea3.fr",
      "phone": "73",
      "mobile": "11",
      "job": "devellopeur",
      "location": "Aix-les-Bains",
      "admin": true,
      "groups": [
        {
            "guid": "2000",
            "gname": "user"
        },
        {
            "guid": "2001",
            "gname": "administrateur"
        },
        {
            "guid": "2002",
            "gname": "web_adm"
        }
      ]
    }
  }
}
```

### Déconnexion

Permet de se déconnecter du LDAP. Nécessite : **cookie de session**.

* URL : http://localhost:5000/api/v1/logout

* Méthode : **GET**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true","message":"Déconnexion effectuée"}
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Non authentifié"}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/logout -b cookie-file.txt -L
```


### Liste des contacts

Permet de récupérer la liste du personnel. Nécessite : **cookie de session**.

* URL : http://localhost:5000/api/v1/contacts

* Méthode : **GET**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true","contacts": [UTILISATEUR_LDAP_1, UTILISATEUR_LDAP_2, .. UTILISATEUR_LDAP_N]}
  ```

* Échec de la requête :
  * Code : 400 BAD REQUEST
    * Contenu: 
    ``` JSON
    {"success":"false","message":"La requête a échouée.", "error": err}
    ```
  * Code : 401 UNAUTHORIZED
    * Contenu: 
    ``` JSON
    {"success":"false","message":"Non authentifié"}
    ```
  * Code : 503 SERVICE UNAVAILABLE
    * Contenu: 
    ``` JSON
    {"success":"false","message":"'Impossible de contacter le service.", "error": err}
    ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/contacts -b cookie-file.txt -L
```

### Récupérer un contact

Permet de récupérer un personnel. Nécessite : **cookie de session**.

* URL : http://localhost:5000/api/v1/contact/:id

* Méthode : **GET**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true","contacts": UTILISATEUR_LDAP}
  ```

* Échec de la requête :
  * Code : 400 BAD REQUEST
    * Contenu: 
    ``` JSON
    {"success":"false","message":"La requête a échouée.", "error": err}
    ```
  * Code : 401 UNAUTHORIZED
    * Contenu: 
    ``` JSON
    {"success":"false","message":"Non authentifié"}
    ```
  * Code : 503 SERVICE UNAVAILABLE
    * Contenu: 
    ``` JSON
    {"success":"false","message":"'Impossible de contacter le service.", "error": err}
    ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/contact/brassoux -b cookie-file.txt -L
```

### Qui suis-je ?

Permet de récupérer l'utilisateur connecté. Nécessite : **cookie de session**.

* URL : http://localhost:5000/api/v1/whoiam

* Méthode : **GET**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true","message":"Authentification effectuée","user": UTILISATEUR_LDAP }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Echec de l'authentification","error":{"message":"Raison"}}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/whoiam -b cookie-file.txt -L
```

### Changer son mot de passe

Permet de changer son mot de passe utilisateur dans le LDAP. Nécessite : **cookie de session**.

* URL : http://localhost:5000/api/v1/changePassword

* Données en paramètres :
  ``` JSON
  {
    "oldPassword": "solea123-",
    "newPassword": "123NewPassword"
  }
  ```

* Méthode : **POST**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true", "message": "Mot de passe changé avec succès." }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Le changement du mot de passe a échoué.","error":{"message":"Raison"}}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/changePassword -b cookie-file.txt -L -d '{"oldPassword": "solea123-", "newPassword": "123NewPassword"}'
```


### Ajouter un contact

Permet d'ajouter un utilisateur dans le LDAP. Nécessite : **cookie de session**, **privilèges administrateur**.

* URL : http://localhost:5000/api/v1/addContact

* Données en paramètres :
  ``` JSON
  {
    "surname": "BRASSOUD",
    "name": "Xavier",
    "job": "Testeur d'API",
    "location": "Chambéry"
  }
  ```

* Méthode : **POST**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true", "contact": UTILISATEUR_LDAP_AJOUTÉ }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED ou 403 FORBIDDEN
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Echec de l'ajout","error":{"message":"Raison"}}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/addContact -b cookie-file.txt -L -d '{"surname": "BRASSOUD", "name": "Xavier", "job": "Testeur", "location": "Chambéry"}'
```

### Ajouter un contact dans des groupes

Permet d'ajouter un utilisateur dans des groupes du LDAP. Nécessite : **cookie de session**, **privilèges administrateur**.

* URL : http://localhost:5000/api/v1/addContactGroups:id

* Données en paramètres :
  ``` JSON
  {
    "groups": [
      "group1",
      "group2"
    ]
  }
  ```

* Méthode : **POST**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true", "contact": UTILISATEUR_LDAP }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED ou 403 FORBIDDEN
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Echec de l'ajout","error":{"message":"Raison"}}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/addContactGroups/brassoux -b cookie-file.txt -L -d '{"groups": ["reseau", "telephonie"]}'
```

### Retirer un contact des groupes

Permet de retirer un utilisateur des groupes du LDAP passé en paramètres. Nécessite : **cookie de session**, **privilèges administrateur**.

* URL : http://localhost:5000/api/v1/deleteContactGroups:id

* Données en paramètres :
  ``` JSON
  {
    "groups": [
      "group1",
      "group2"
    ]
  }
  ```

* Méthode : **POST**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true", "contact": UTILISATEUR_LDAP }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED ou 403 FORBIDDEN
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Echec du retrait.","error":{"message":"Raison"}}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/deleteContactGroups/brassoux -b cookie-file.txt -L -d '{"groups": ["reseau", "telephonie"]}'
```

### Modifier un contact

Permet de modifier un utilisateur dans le LDAP. Nécessite : **cookie de session**, **privilèges administrateur**.

* URL : http://localhost:5000/api/v1/editContact/[UID_UTILISATEUR]

* Données en paramètres (OPTIONNELS) :
  ``` JSON
  {
    "surname": "BRASSOUD",
    "name": "Xavier",
    "job": "Testeur d'API",
    "location": "Chambéry"
  }
  ```

* Méthode : **POST**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true", "contact": UTILISATEUR_LDAP_MODIFIÉ }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED ou 403 FORBIDDEN
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Echec de l'ajout","error":{"message":"Raison"}}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/editContact/brassoux -b cookie-file.txt -L -d '{"surname": "BRASSOUD", "name": "Xavier"}'
```

### Ajouter/Retirer les privilèges administrateur d'un contact

Permet d'ajouter/retirer les privilèges administrateur d'un contact. Nécessite : **cookie de session**, **privilèges administrateur**.

* URL : http://localhost:5000/api/v1/grantAdmin/[UID_UTILISATEUR]

* Données en paramètres :
  ``` JSON
  {
    "admin": true/false
  }
  ```

* Méthode : **POST**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true", "contact": UTILISATEUR_LDAP_MODIFIÉ }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED ou 403 FORBIDDEN
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Echec de la modification","error":{"message":"Raison"}}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/grantAdmin/brassoux -b cookie-file.txt -L -d '{"admin": true}'
```


### Retirer un contact

Permet de retirer un utilisateur du LDAP. Nécessite : **cookie de session**, **privilèges administrateur**.

* URL : http://localhost:5000/api/v1/deleteContact/[UID_UTILISATEUR]

* Méthode : **DELETE**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true", "message": "Contact correctement retiré de l'annuaire LDAP." }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED ou 403 FORBIDDEN
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Echec de la modification","error":{"message":"Raison"}}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/deleteContact/brassoux -b cookie-file.txt -L
```


### Réinitialiser le mot de passe d'un contact

Permet réinitialiser le mot de passe d'un utilisateur du LDAP. Nécessite : **cookie de session**, **privilèges administrateur**.

* URL : http://localhost:5000/api/v1/resetPassword/[UID_UTILISATEUR]

* Méthode : **POST**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true", "message": "Le mot de passe a correctement été réinitialisé." }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED ou 403 FORBIDDEN
  * Contenu: 
  ``` JSON
  {"success":"false","message":"La réinitialisation du mot de passe a échoué. Paramètres incorrectes.","error":{"message":"Raison"}}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/resetPassword/brassoux -b cookie-file.txt -L
```

### Importer une nouvelle photo de profil

Permet d'importer une photo de profil. Nécessite : **cookie de session**.

* URL : http://localhost:5000/api/v1/upload

* Méthode : **POST**

* Données en paramètres :
  ``` html
  <form method="post" enctype="multipart/form-data" action="/upload">
    <input type="file" name="image">
    <input type="submit" value="Submit">
  </form>
  ```

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true", "message": "La photo de profil a correctement été importée." }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Impossible d'importer la photo de profil."}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/upload -b cookie-file.txt -L
```


### Supprimer sa photo de profil

Permet de supprimer sa photo de profil. Nécessite : **cookie de session**.

* URL : http://localhost:5000/api/v1/deleteProfil

* Méthode : **DELETE**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true", "message": "La photo de profil a correctement été supprimée." }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Impossible de supprimer la photo de profil."}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/deleteProfil -b cookie-file.txt -L
```

### Supprimer la photo de profil d'un autre utilisateur

Permet de supprimer la photo de profil d'un autre utilisateur. Nécessite : **cookie de session**, **privilèges administrateur**.

* URL : http://localhost:5000/api/v1/deleteProfil/:id

* Méthode : **DELETE**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true", "message": "La photo de profil a correctement été supprimée." }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Impossible de supprimer la photo de profil."}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/deleteProfil/brassoux -b cookie-file.txt -L
```


### Click to Call

Permet d'appeler un numéro. Nécessite : **cookie de session**.

* URL : http://localhost:5000/api/v1/call/:id

* Méthode : **POST**

* Succès de la requête :
  * Code : 200
  * Contenu: 
  ``` JSON
  {"success":"true", "message": "Appel correctement effectué." }
  ```

* Échec de la requête :
  * Code : 401 UNAUTHORIZED ou 404
  * Contenu: 
  ``` JSON
  {"success":"false","message":"Une erreur est survenue lors de l'appel."}
  ```

* Exemple d'appel :
``` bash
curl -X GET http://localhost:5000/api/v1/call/73 -b cookie-file.txt -L
```
